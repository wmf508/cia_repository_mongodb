package node

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"

	commonNode "gitlab.com/wmf508/alfredy/kubernetes/node"
	commonNodeRepo "gitlab.com/wmf508/cia/repository/node"
)

type NodeRepository struct {
	Db *mongo.Client
}

// create node repository instance
func NewNodeRepository(db *mongo.Client) commonNodeRepo.NodeRepository {

	repo := &NodeRepository{
		Db: db,
	}

	return repo
}

// update node in mongodb, create new doucment if no exist.
// 返回被修改和添加的数据总数与错误
func (repo *NodeRepository) UpdateNodesCreateNoExist(nodes []*commonNode.Node) (int64, error) {
	var modifyCounts int64
	var upsertCounts int64
	errs := []string{}
	for _, no := range nodes {
		filter := bson.M{"hostname": no.Hostname}

		updateByte, err := bson.Marshal(no)
		if err != nil {
			errs = append(errs, fmt.Errorf("node: %s: %w", no.Hostname, err).Error())
			continue
		}

		var update bson.M
		err = bson.Unmarshal(updateByte, &update)
		if err != nil {
			errs = append(errs, fmt.Errorf("node: %s: %w", no.Hostname, err).Error())
			continue
		}

		opts := options.Update().SetUpsert(true)

		result, err := repo.Db.Database("slumdunk").Collection("asset_node").UpdateOne(context.TODO(), filter, bson.M{"$set": update}, opts)
		if err != nil {
			errs = append(errs, fmt.Errorf("node: %s: %w", no.Hostname, err).Error())
		} else {
			modifyCounts += result.ModifiedCount
			upsertCounts += result.UpsertedCount
		}
	}

	// log.Printf("%d nodes was updated", modifyCounts)
	// log.Printf("%d nodes was inserted", upsertCounts)

	total := modifyCounts + upsertCounts

	if len(errs) > 0 {
		return total, errors.New(strings.Join(errs, "\n"))
	}

	return total, nil
}
