package node_test

import (
	"net"
	"testing"

	"github.com/stretchr/testify/require"
	commonNode "gitlab.com/wmf508/alfredy/kubernetes/node"
	commonRepo "gitlab.com/wmf508/cia/repository"
	"gitlab.com/wmf508/cia_repository_mongodb/internal/test"
)

var (
	repo *commonRepo.Repository
)

func TestMain(m *testing.M) {
	repo, _ = test.GetRepository()

	m.Run()
}

func TestUpdateNodesCreateNoExist(t *testing.T) {
	node1 := &commonNode.Node{
		Hostname:   "test-111",
		InternalIp: net.IPv4(192, 168, 1, 3).String(),
		Status:     1,
	}

	node2 := &commonNode.Node{
		Hostname:   "test-222",
		InternalIp: net.IPv4(192, 168, 1, 6).String(),
		Status:     3,
	}

	nodes := []*commonNode.Node{node1, node2}

	cIds, err := repo.Node.UpdateNodesCreateNoExist(nodes)
	require.NoError(t, err)
	require.NotEmpty(t, cIds)
	require.Equal(t, int64(2), cIds)

	// TODO: remove node1 node2 from mongodb
}
