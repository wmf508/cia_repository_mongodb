package repository

import (
	"log"

	storageMongo "gitlab.com/wmf508/alfred/database/mongodb"
	commonRepo "gitlab.com/wmf508/cia/repository"
	"gitlab.com/wmf508/cia_repository_mongodb/internal/db"
	"gitlab.com/wmf508/cia_repository_mongodb/repository/node"
)

// 汇集所有类型的repository到mainRepo中
func NewRepository(config storageMongo.Config) (*commonRepo.Repository, error) {
	mainRepo := &commonRepo.Repository{}

	db, err := db.NewDbClient(config)
	if err != nil {
		log.Println("failed to create mongodb client")
		return nil, err
	}
	nodeRepo := node.NewNodeRepository(db)

	mainRepo.Node = nodeRepo

	return mainRepo, nil
}
