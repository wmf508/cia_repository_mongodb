package db

import (
	"go.mongodb.org/mongo-driver/mongo"

	storageMongo "gitlab.com/wmf508/alfred/database/mongodb"
)

// create mongo db client
func NewDbClient(config storageMongo.Config) (*mongo.Client, error) {
	db, err := storageMongo.GetClient(config)

	if err != nil {
		return nil, err
	}

	return db, nil
}
