package db

import (
	"testing"

	"github.com/stretchr/testify/require"
	storageMongo "gitlab.com/wmf508/alfred/database/mongodb"
)

func TestNewDbClient(t *testing.T) {
	config := storageMongo.Config{}
	db, err := NewDbClient(config)
	require.NoError(t, err)
	require.NotEmpty(t, db)
}
