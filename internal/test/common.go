package test

import (
	"log"

	storageMongo "gitlab.com/wmf508/alfred/database/mongodb"
	commonRepo "gitlab.com/wmf508/cia/repository"
	"gitlab.com/wmf508/cia_repository_mongodb/repository"
)

var TestConfig = storageMongo.Config{
	Address:  "127.0.0.1",
	Port:     27017,
	Username: "admin",
	Password: "12345",
}

func GetRepository() (*commonRepo.Repository, error) {
	repo, err := repository.NewRepository(TestConfig)
	if err != nil {
		log.Println("failed to create repo")
		return nil, err
	}
	return repo, nil
}
