module gitlab.com/wmf508/cia_repository_mongodb

go 1.17

require (
	github.com/stretchr/testify v1.7.1
	gitlab.com/wmf508/alfred v0.0.0-20220403040954-51e1c6781c31
	gitlab.com/wmf508/alfredy v0.0.0-20220403144052-b5ba363b9ca8
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)

require gitlab.com/wmf508/arksec_common v0.0.0-20220331145052-0bc42b93b720 // indirect

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	gitlab.com/wmf508/cia v0.0.0-20220403074252-196028f7e5ad
	go.mongodb.org/mongo-driver v1.8.4
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b // indirect
	golang.org/x/mobile v0.0.0-20190719004257-d2bd2a29d028 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
